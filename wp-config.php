<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'coady_cms');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'm6buprh2');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=ucj.H]@7gksJ8wil=s5u6ODn|JS&V/$a{q+5d6:SgO]|/nX{0izMt8KVMThtJ6I');
define('SECURE_AUTH_KEY',  't&lcDgrGSu]$Sq/w8X0INs B|uK=cz[hdN5yM!oo$ZPM7KY:`64>Fpmn3)uS$np6');
define('LOGGED_IN_KEY',    ' I :{t$P2wa:aH$zNQv15&MvN*=)+Y.^}-4UUTgCPk}n-JV)5$HH5Fj#0_3lWo38');
define('NONCE_KEY',        'QP>E/`:}_Yr^6}G(aOCiw$iS-tM$`U!Gn-Z=PtKf T[L<;AIoKFJ|6e-0gH?`i#n');
define('AUTH_SALT',        'V|?}r92yb|(ay(@yT$cH4}<Kagm:bP!wtJR! yJuoxb>sBwZDVd{tnS5LY *Wa#.');
define('SECURE_AUTH_SALT', 'i3BUK[dtl dxm`HGF{M&fP^kT}xe.^.=EipXm:a0,[P<`W%c{>:E3_a$|r`Y?,j-');
define('LOGGED_IN_SALT',   '*4?dB.cs1ZAcj(d<iEv*{@sHh2H<O$Rt?~Sb:8T3b 3<Pqk{dG2qW iod,xxy627');
define('NONCE_SALT',       'lo*T~QWERE*v1~CS rpZch]fX4V#9_vOe^k`BHAseC4(z)$U{RFFt%I!/z?W.R/)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
