<?php

	foreach (glob(get_template_directory() . '/classes/*.php') as $filename) {
	    require_once $filename;
	}

	Theme::init();
	Debug::init();

	add_action( 'init', 'my_remove_post_type_support', 999 );
	function my_remove_post_type_support() {
	    remove_post_type_support( 'page', 'editor' );
	}


	// function acf_load_sample_field( $field ) {
	//    $field['choices'] = get_post_type_values(['windowranges', 'doorranges']);
	//     return $field;
	// }
	// add_filter( 'acf/load_field/name=category', 'acf_load_sample_field' );
	// function get_post_type_values( $post_type ) {
	//     $values = array();
	//     $defaults = array(
	//                         'post_type' => $post_type,
	//                         'post_status' => 'publish',
	//                         'posts_per_page' => -1,
	//                         'orderby' => 'title',
	//                         'order' => 'ASC'
	//                     );
	//     $query = new WP_Query( $defaults );
	//     if ( $query->found_posts > 0 ) {
	//         foreach ( $query->posts as $post ) {
	//           $values[get_the_title( $post->ID )] = get_the_title( $post->ID );
	//         }
	//     }
	//     return $values;
	// }

	function my_acf_init() {
		acf_update_setting('google_api_key', 'AIzaSyD9ISEjEXewZWfWX8lFAwG0h_YsZGKrO9w');
	}

	my_acf_init();

?>