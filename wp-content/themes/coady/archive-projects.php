<?php

get_header();


	$productArgs = array(
        'post_type' => 'products',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $productQuery = new WP_Query($productArgs);
    $products = $productQuery->get_posts();

	$projectArgs = array(
        'post_type' => 'projects',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $projectQuery = new WP_Query($projectArgs);
    $projects = $projectQuery->get_posts();

?>


<main id="project-archive">
	<div class="container">
		<h1>Projects</h1>

		<div id="products-navigation" class="container">
			<h4>Swipe Left &amp; Right for full product range!</h4>
		</div>
		<div id="products-container">
			<button data-filter="*">All</button>
			<?php
				foreach($products as $product) {

					$productTitle = $product->post_title;
					$productTitleTrim = preg_replace('/\s*/', '', $productTitle);
					$productTitleTrim = strtolower($productTitleTrim);

					echo '<button data-filter=".' . $productTitleTrim . '">' . $product->post_title . '</button>';
				}
			?>
		</div>
		<div id="projects-container">
			<?php 
				
				foreach($projects as $project) {

					if(have_rows('projects_page', $project->ID)): while (have_rows('projects_page', $project->ID) ) : the_row();

						$image = get_sub_field('images')[0]['image']['url'];
						$categories = get_sub_field('products');

						$categoryString = '';

						foreach($categories as $category) {
							$category = $category['product'];

							$category = trim(substr($category, strpos($category, 'products') + 8));
							$category = strtolower($category);
							$category = str_replace("-", "", $category);
							$category = str_replace("/", "", $category);
							$categoryString .= $category . ' ';

						}


					endwhile; endif;

						echo '
							<div class="archive-project ' . $categoryString . '">
								<h3>// ' . $project->post_title . '</h3>
								<a href="' . $project->guid . '" class="section-image" style="background-image:url(' . $image . ');">
									<div class="project-overlay"><span>VIEW PROJECT <img src="' . get_template_directory_uri() . '/assets/images/arrow-right.png"/></span></div>
								</a>
							</div>
						';
		

				}

			?>
		</div>

	</div>
</main>


<?php
	get_footer();
?>
