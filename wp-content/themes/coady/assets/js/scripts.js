jQuery(document).ready(function(){

	var body = jQuery('body.home');
	jQuery(window).scroll(function () {
	    if (jQuery(this).scrollTop() === 0) {
	        jQuery(body).removeClass("sticky");
	    }

	    else if (jQuery(this).scrollTop() > 1) {
	        jQuery(body).addClass("sticky");
	    }
	});

	// Menu 

		jQuery('#mobile-menu').click(function(){
			jQuery('header').toggleClass('active');
		});

		if(window.innerWidth < 960) {
			jQuery('nav ul li.menu-item-has-children').click(function(){
				jQuery(this).find('> ul').slideToggle('fast');
			});
		}
		

	var sliders = []; 

	if (jQuery('.main-banner .carousel-cell').length > 1) {
		sliders.push('.main-banner .carousel');
	}

	if (jQuery('#home-testimonials .carousel-cell').length > 1) {
		sliders.push('#home-testimonials .carousel');
	}

	var s = sliders.join(", ");

	jQuery(s).flickity({
		autoPlay: 5000,
		selectedAttraction: 0.01,
		friction: 0.15
	});

	jQuery('.projects-slider .carousel').flickity({
		autoPlay: 5000,
		arrowShape: {
			x0: 5,
			x1: 20, y1: 15,
			x2: 20, y2: 0,
			x3: 100
		}
	});

	jQuery('#home-partners .carousel').flickity({
		autoPlay: 3000,
		wrapAround: true,
		selectedAttraction: 0.01,
	});

	// Product Slider (custom slider nav)

		var carousel = jQuery('#product-slider').flickity({
		  pageDots: false
		});

		var flkty = carousel.data('flickity');

		var cellButtonGroup = jQuery('#product-slider-nav');
		var cellButtons = cellButtonGroup.find('.section-image');

		carousel.on( 'select.flickity', function() {
		  cellButtons.filter('.is-selected')
		    .removeClass('is-selected');
		  cellButtons.eq( flkty.selectedIndex )
		    .addClass('is-selected');
		});

		cellButtonGroup.on( 'click', '.section-image', function() {
		  var index = jQuery(this).index();
		  carousel.flickity( 'select', index );
		});

	// Prevent Default on nav hash

		jQuery('nav a').click(function(e){
			if(jQuery(this).attr('href') === '#') {
				e.preventDefault();
			}
		});



		jQuery(window).load(function(){
			jQuery('.grid').masonry({
				percentPosition: true,
				columnWidth: '.grid-item',
				itemSelector: '.grid-item',
				gutter: '.gutter-sizer',
				isAnimated: true,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});
		});

		var $grid = jQuery('#projects-container').isotope({
		  // options
		});

		jQuery('#products-container').on( 'click', 'button', function() {
		  var filterValue = jQuery(this).attr('data-filter');
		  $grid.isotope({ filter: filterValue });
		  jQuery(this).addClass('active');
		  jQuery('#products-container button').not(this).removeClass('active');
		});

});
