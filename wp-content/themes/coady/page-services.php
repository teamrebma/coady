<?php
// Template Name: Services

get_header();

?>
<main id="services-page">
	<?php if( have_rows('services_page') ): while ( have_rows('services_page') ) : the_row(); 
		$services = get_sub_field('service_pod');
		$serviceContent = get_sub_field('content_section');
	?>

		<div class="container"><h1><?php echo get_sub_field('page_title'); ?></h1></div>
		<div id="services-container">
			<?php
				$i = 1;

				foreach($services as $service) {
					echo '
						<div class="service-wrapper">
							<div class="container">
								<div class="section-image" style="background-image: url(' . $service['image']['url'] . ');"></div>
								<div class="service-content">
									<img class="service-icon" src="' . $service['icon']['url'] . '" />
									'. ($service['title'] ? '<h2>0' . $i . '. ' . $service['title'] . '</h2>' : '') . '
									'. ($service['heading'] ? '<h3>' . $service['heading'] . '</h3>' : '') .'
									'. ($service['content'] ? '<p>' . $service['content'] . '</p>' : '') .'
								</div>
							</div>
						</div>
					';

					$i++;
				}

				echo '</div>';

				if($serviceContent) {
					foreach($serviceContent as $content) {
						echo '
							<div class="content-blurb">
								<div class="container">
									'. ($content['content'] ? '<p>' . $content['content'] . '</p>' : '') . '';

									if($content['button'][0]['enable_button'][0] === 'Yes') {
										$button = $content['button'][0];

										if($button['button_title'] && $button['button_url']) {
											echo '<a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a>';
										}
									}

							echo '</div>
							</div>
						';
					}
				}
			?>	
	<?php endwhile; endif; ?>
</main>


<?php
get_footer();
?>
