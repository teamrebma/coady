<footer>
	<div id="footer-pods" class="container">
		<div class="footer-pod">
			<h4>About</h4>
			<?php the_field('footer_about', 'options') ?>
		</div>
		<div class="footer-pod footer-links">
			<div class="links-wrapper">
				<h4>Help</h4>
				<?php
					wp_nav_menu(
					        array(
					            'container' => false,
					            'menu' => __('Help Menu'),
					            'theme_location' => 'help-links',
					        )
					);
				?> 
			</div>
			<div class="links-wrapper">
				<h4>Info</h4>
				<?php
					wp_nav_menu(
					        array(
					            'container' => false,
					            'menu' => __('Info Menu'),
					            'theme_location' => 'info-links',
					        )
					);
				?>
			</div>
		</div>
		<div class="footer-pod">
			<h4>Contact</h4>
			<p><?php the_field('contact_address', 'options') ?></p>
			<p><a href="tel:+61<?php the_field('contact_phone', 'options') ?>">(+61) <?php the_field('contact_phone', 'options') ?></a></p>
			<p><a href="mailto:<?php the_field('contact_email', 'options') ?>"><?php the_field('contact_email', 'options') ?></a></p>
		</div>
	</div>
	<div id="footer-about">
		<div class="container">
			<p id="footer-copyright">&copy; <?php echo date('Y'); ?> Coady by Design</p>
			<div id="footer-social">
				<?php if (get_field('footer_facebook', 'options')): ?>
					<a href="<?php the_field('footer_facebook', 'options') ?>"><i class="fa fa-facebook"></i></a>
				<?php endif; ?>
				<?php if (get_field('footer_twitter', 'options')): ?>
					<a href="<?php the_field('footer_twitter', 'options') ?>"><i class="fa fa-twitter"></i></a>
				<?php endif; ?>
				<?php if (get_field('footer_linkedin', 'options')): ?>
					<a href="<?php the_field('footer_linkedin', 'options') ?>"><i class="fa fa-linkedin"></i></a>
				<?php endif; ?>
			</div>
			<p id="footer-thrive">Site by <a href="https://thriveweb.com.au/" target="_blank">Thrive</a></p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>