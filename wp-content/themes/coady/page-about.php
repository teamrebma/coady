<?php
// Template Name: About Page
get_header();

?>
<main id="about-page">
	
	<?php if( have_rows('about_banner') ): while ( have_rows('about_banner') ) : the_row(); 
		$aboutBanners = get_sub_field('slider_pod');
	?>
		<div id="about-banner" class="main-banner">
			<div class="triangle triangle-left"></div>
			<div class="triangle triangle-right"></div>
			<div class="carousel">
				<?php
				$x = count($aboutBanners);
				$i = 1;

				foreach($aboutBanners as $aboutBanner) {
					echo '
					<div class="banner-slide carousel-cell">
						<div class="slide-image" style="background-image: url(' . $aboutBanner['image']['url'] . ');">
							<div class="banner-content container">
								'. ($x > 1 ? '<p class="banner-counter"><span>0' . $i .'</span> / 0' . $x . '</p>' : '') .'
								' . ($aboutBanner['title'] ? '<p class="banner-title">' . $aboutBanner['title'] . '</p>' : '') . '
								' . ($aboutBanner['content'] ? '<p>' . $aboutBanner['content'] . '</p>' : '') . '';

								if($aboutBanner['button'][0]['enable_button'][0] === 'Yes') {
									$button = $aboutBanner['button'][0];

									if($button['button_title'] && $button['button_url']) {
										echo '<a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a>';
									}
								}
					echo '	</div>
						</div>
					</div>';

					$i++;
				}
			?>
			</div>
		</div>
	<?php endwhile; endif; ?>

	<?php if( have_rows('main_content') ): while ( have_rows('main_content') ) : the_row(); 

		$contentTitle = get_sub_field('title');
		$contentText = get_sub_field('content');

	?>

		<div id="about-main">
			<div class="triangle triangle-left triangle-dark"></div>
			<div class="container">
				<?php if($contentTitle) { ?><h2><?php echo $contentTitle; ?></h2><?php } ?>
				<?php if($contentText) { ?><p><?php echo $contentText; ?></p><?php } ?>
			</div>
		</div>


	<?php endwhile; endif; ?>

	<?php if( have_rows('secondary_content') ): while ( have_rows('secondary_content') ) : the_row(); 

		$contentTitle = get_sub_field('title');
		$contentText = get_sub_field('content');
		$contentImage = get_sub_field('image');

	?>

		<div id="about-secondary">
			<div class="container">
				<div class="content-container">
					<?php if($contentTitle) { ?><h2><?php echo $contentTitle; ?></h2><?php } ?>
					<?php if($contentText) { ?><p><?php echo $contentText; ?></p><?php } ?>
				</div>
				<div class="section-image" style="background-image:url(<?php echo $contentImage['url']; ?>);"></div>
			</div>
		</div>


	<?php endwhile; endif; ?>

	<?php if( have_rows('about_projects') ): while ( have_rows('about_projects') ) : the_row(); 
		$projectTitle = get_sub_field('featured_title');
		$projectText = get_sub_field('featured_text');
		$projectImage = get_sub_field('featured_image');
		$projectPods = get_sub_field('project_pod');
	?>
		<div id="about-projects">
			<div class="container">
				<div id="project-wrapper">
					<div id="project-content">
						<?php if($projectTitle) { ?><h2><?php echo $projectTitle; ?></h2><?php } ?>
						<?php if($projectText) { ?><p><?php echo $projectText; ?></p><?php } ?>
					</div>
					<div class="section-image" <?php if($projectImage['url']) { ?> style="background-image:url(<?php echo $projectImage['url']; ?>);" <?php } ?>></div>
				</div>

				<?php if($projectPods) { ?>
				<h3>Featured work</h3>
				<div class="projects-slider">
					<?php
						
							$x = count($projectPods);
							$i = 1;
							echo '<div class="carousel flickity-fade">';
								foreach($projectPods as $projectPod) {
									echo '
										<div class="carousel-cell">
											'. ($x > 1 ? '<p class="project-counter"><span>0' . $i .' | </span> 0' . $x . '</p>' : '') .'
											'. ($projectPod['title'] ? '<h4>' . $projectPod['title'] . '</h4>' : '') .'
											'. ($projectPod['content'] ? '<p>' . $projectPod['content'] . '</p>' : '') .'
											'. ($projectPod['url'] ? '<a class="button" href="' . $projectPod['url'] . '">VIEW PROJECT</a>' : '') . '
										</div>
									';

									$i++;
								}
							echo '</div>
							</div>';
						}
					?>
			</div>
		</div>
	<?php endwhile; endif; ?>
	
</main>

<?php
get_footer();
?>
