<?php
// Template Name: Single Project

get_header();

?>
<main id="project-page">

		<?php if( have_rows('projects_page') ): while ( have_rows('projects_page') ) : the_row(); 
			$projectProducts = get_sub_field('products');
			$projectContent = get_sub_field('project_overview');
			$projectImages = get_sub_field('images');
		?>

			<div class="container">
				<h1>Projects</h1>

				<div id="project-info">
					<h2><?php the_title(); ?></h2>

					<div id="project-products">
						<h3>Products</h3>

						<?php
							foreach($projectProducts as $projectProduct) {
								$productName = $projectProduct['product'];
								
								$productName = trim(substr($productName, strpos($productName, 'products') + 8));
								$productName = str_replace("-", " ", $productName);
								$productName = str_replace("/", "", $productName);

								
								echo '<p><a href="' . $projectProduct['product'] . '">' . $productName . '</a></p>';
							}
						?>
				</div>

				<div id="project-overview">
					<h3>Project overview</h3>
					<p><?php echo $projectContent; ?></p>
				</div>
			</div>

			<div>
				<div class="grid">
					<div class="gutter-sizer"></div>
					<?php

						foreach($projectImages as $projectImage) {
							echo '<img class="grid-item" src="' . $projectImage['image']['url'] . '" />';
						}

					?>
				</div>
			</div>

		<?php endwhile; endif; ?>

</main>


<?php
get_footer();
?>
