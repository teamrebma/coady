<?php
// Template Name: Contact

get_header();

?>
<main id="contact-page">
	<?php if( have_rows('contact_page') ): while ( have_rows('contact_page') ) : the_row(); 
		$contactTitle = get_sub_field('title');
		$contactContent = get_sub_field('content');
		$contacImage = get_sub_field('image');
	?>

	<div class="container">
		<h1><?php echo $contactTitle; ?></h1>
	</div>

	<div id="contact-about">
		<div class="container">
			<div id="about-content">
				<div class="triangle triangle-right triangle-dark"></div>
				<?php if($contactContent){ echo $contactContent; }?>
				<div id="contact-location">
				<?php echo '
					'. (get_field('contact_address', 'options') ? '<p>' . get_field('contact_address', 'options') . '</p>' : '') . '
					'. (get_field('contact_phone', 'options') ? '<p><a href="tel:+61' . get_field('contact_phone', 'options') . '">(+61) '. get_field('contact_phone', 'options') . '</a></p>' : '') .' 
					'. (get_field('contact_email', 'options') ? '<p><a href="mailto:'. get_field('contact_email', 'options') .'">'. get_field('contact_email', 'options') .'</a></p>' : ''). '
				</div>
				'; ?>
			</div>
			<div class="section-image" <?php if($contacImage){ ?> style="background-image:url('<?php echo $contacImage['url']; ?>');" <?php } ?>></div>
		</div>
	</div>

	<div id="contact-hours">
		<div class="container">
			<div id="contact-map"></div>
			<div id="hours-content">
				<div class="triangle triangle-right triangle-dark"></div>
				<h3>Open hours</h3>
				<?php if(get_field('open_hours', 'options')){ ?><p><?php echo get_field('open_hours', 'options'); ?></p> <?php } ?> 
			</div>
		</div>
	</div>
	
	<?php endwhile; endif; ?>
</main>


<?php

get_template_part('template-parts/map', 'page');

get_footer();
?>
