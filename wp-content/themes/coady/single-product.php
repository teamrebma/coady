<?php
// Template Name: Single Product

get_header();

?>
<main id="product-page">

		<?php if( have_rows('product_page') ): while ( have_rows('product_page') ) : the_row(); 
			$productAbout = get_sub_field('about');
			$productProjects = get_sub_field('related_projects');
			$productSliders = get_sub_field('slider');

			$productContents = get_sub_field('content_section');

		?>


		<div class="container">

			<h1><?php the_title(); ?></h1>

			<div id="product-description">
				<?php if($productAbout){ ?><p><?php echo $productAbout; ?></p><?php } ?>

				<div id="related-projects">
					<?php if($productProjects) {
						echo '<h3>Related Projects</h3>';

							foreach($productProjects as $productProject) {

								$productTitle = trim(substr($productProject['project'], strpos($productProject['project'], 'projects/') + 9));
								$productTitle = str_replace("-", " ", $productTitle);
								$productTitle = str_replace("/", "", $productTitle);

								if($productProject['project'] && $productTitle){
									echo '<p><a href="' . $productProject['project'] . '">' . $productTitle . '</a></p>';
								}
							}
						}
					?>
				</div>
			</div>
		</div>

		<?php if($productSliders) { ?>
			<div id="product-slider-wrapper">
				<div class="container">
					<div id="product-slider-container">
						<div id="product-slider">
							<?php
								foreach($productSliders as $productSlider) {
									if($productSlider['image']) {
										echo '<div class="section-image" style="background-image: url(' . $productSlider['image']['url'] . ');"></div>';
									}
								}
							?>
						</div>
						<div class="triangle triangle-right triangle-dark"></div>
					</div>

					<div id="product-slider-nav">
						<?php

							foreach($productSliders as $productSlider) {
								if($productSlider['image']) {
									echo '<div class="section-image" style="background-image: url(' . $productSlider['image']['url'] . ');"></div>';
								}
							}
						?>
					</div>
				</div>
			</div>
		<?php } ?>

		<?php if($productContents[0]['content']) { ?>
			<div id="product-blurb">
				<div class="container">
					<?php
						foreach($productContents as $productContent) {
							if($productContent['content']) {
								echo '<p>' . $productContent['content'] . '</p>';
							}
							if($productContent['button'][0]['enable_button'][0] === 'Yes') {
								$button = $productContent['button'][0];

								if($button['button_title'] && $button['button_url']) {
									echo '<a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a>';
								}
							}
						}
					?>
				</div>
			</div>
		<?php } ?>

		<?php endwhile; endif; ?>
</main>


<?php
get_footer();
?>
