<?php

class Theme {

    public static function init() {
        // Assets
        add_action('wp_print_styles', 'Theme::loadAssets');
        // Menus
        add_action('after_setup_theme', 'Theme::registerMenus');
        // Sidebars
        // add_action('widgets_init', 'Theme::registerSidebars');

        Theme::customPostTypes();

        // Option Pages
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page('Options');
        }
        return;

    }

    public static function loadAssets() {
        global $wp_styles;
        if (!is_admin()) {
            wp_enqueue_script('jquery');

            wp_register_style('flickity-css', "https://unpkg.com/flickity@2/dist/flickity.min.css");
            wp_register_style('font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css");
            wp_register_style('main-styles', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0.0');



            wp_register_script('flickity-js', "https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js");
            wp_register_script('isotope-js', get_stylesheet_directory_uri() . '/assets/js/isotope.min.js', array('jquery'), '1.0.0',true);
            wp_register_script('masonary-js', get_stylesheet_directory_uri() . '/assets/js/masonary.min.js', array('jquery'), '1.0.0',true);
            wp_register_script('main-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), '1.0.1',true);
         
            wp_enqueue_style('flickity-css');
            wp_enqueue_style('font-awesome');
            wp_enqueue_style('main-styles');


            wp_enqueue_script('isotope-js');
            wp_enqueue_script('flickity-js');
            wp_enqueue_script('masonary-js');
            wp_enqueue_script('main-js');

        }
        return;
    }

    public static function registerMenus() {
        // wp menus
        add_theme_support('menus');
        register_nav_menus(
                array(
                    'main-nav' => __('The Main Menu'), // main nav in header                    
                    'footer-nav' => __('Footer Menu'), // secondary nav in footer
                    'help-links' => __('Help Menu'),
                    'info-links' => __('Info Menu'),
                )
        );
        return;
    }

    public static function customPostTypes() {

            function custom_projects() {
                $supports = array('title');
                register_post_type( 'projects',
                    array(
                        'labels' => array(
                            'name' => __( 'Projects' ),
                            'singular_name' => __( 'Project' )
                        ),
                        'public' => true,
                        'description' => __('This is the projects post type', 'coady'),
                        'has_archive' => true,
                        'rewrite' => array('slug' => 'projects'),
                        'menu_icon' => 'dashicons-category',
                        'supports' => $supports,

                    )
                );
            }

            function custom_products() {
                $supports = array('title');
                register_post_type( 'products',
                    array(
                        'labels' => array(
                            'name' => __( 'Products' ),
                            'singular_name' => __( 'Product' )
                        ),
                        'public' => true,
                        'description' => __('This is the products post type', 'coady'),
                        'has_archive' => true,
                        'rewrite' => array('slug' => 'products'),
                        'menu_icon' => 'dashicons-hammer',
                        'supports' => $supports,
                        'taxonomies' => array( 'category' ),

                    )
                );
            }

            add_action( 'init', 'custom_projects' );
            add_action( 'init', 'custom_products' );

            add_filter( 'template_include', function( $template ) {

                $my_types = array('products' );
                $post_type = get_post_type();

                if ( ! in_array( $post_type, $my_types ) )
                    return $template;

                return get_stylesheet_directory() . '/single-product.php'; 
            });


        return;
    }
}

?>