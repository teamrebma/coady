<?php

$map = get_field('map', 'options');

?>

<script type="text/javascript">
	
	function initMap() {
	    var map = new google.maps.Map(document.getElementById('contact-map'), {
	        zoom: 14,
	        center: new google.maps.LatLng(<?php echo $map['lat']; ?>, <?php echo $map['lng']; ?>),
	        disableDefaultUI: true,
	        zoomControl: true,
	        scrollwheel: false,
	        draggable: !("ontouchend" in document),
	        styles: [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "poi", "elementType": "labels.text", "stylers": [{"visibility": "off"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#dbdbdb"}, {"visibility": "on"}]}]
	    });
		var Icon = { 
	        url: '/wp-content/themes/coady/assets/icons/map-icon.png',
	    };
	    var marker = new google.maps.Marker({
	      position: {lat: <?php echo $map['lat']; ?>, lng: <?php echo $map['lng']; ?>},
	      map: map,
	      icon: Icon,
	    });
	}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9ISEjEXewZWfWX8lFAwG0h_YsZGKrO9w&callback=initMap"
    async defer></script>
