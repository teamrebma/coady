<?php
// Template Name: Home
get_header();

?>
<main>
	<?php if( have_rows('home_banner') ): while ( have_rows('home_banner') ) : the_row(); 
		$homeBanners = get_sub_field('slider_pod');
	?>
		<div id="home-banner" class="main-banner">
			<div class="triangle triangle-left"></div>
			<div class="triangle triangle-right"></div>
			<div class="carousel">
				<?php
				$x = count($homeBanners);
				$i = 1;

				foreach($homeBanners as $homeBanner) {
					echo '
					<div class="banner-slide carousel-cell">
						<div class="slide-image" style="background-image: url(' . $homeBanner['image']['url'] . ');">
							<div class="banner-content container">
								'. ($x > 1 ? '<p class="banner-counter"><span>0' . $i .'</span> / 0' . $x . '</p>' : '') .'
								' . ($homeBanner['title'] ? '<p class="banner-title">' . $homeBanner['title'] . '</p>' : '') . '
								' . ($homeBanner['content'] ? '<p>' . $homeBanner['content'] . '</p>' : '') . '';

								if($homeBanner['button'][0]['enable_button'][0] === 'Yes') {
									$button = $homeBanner['button'][0];

									if($button['button_title'] && $button['button_url']) {
										echo '<a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a>';
									}
								}
					echo '	</div>
						</div>
					</div>';

					$i++;
				}
			?>
			</div>
		</div>
	<?php endwhile; endif; ?>
	
	<?php if( have_rows('home_about') ): while ( have_rows('home_about') ) : the_row(); ?>
		<div id="home-about">
			<div class="container-large">
				<div class="triangle triangle-left triangle-dark"></div>
				<?php if (get_sub_field('title')) { ?><h2><?php echo get_sub_field('title'); ?></h2><?php } ?>
				<?php if (get_sub_field('content')) { ?><p><?php echo get_sub_field('content'); ?></p><?php } ?>
				<?php
					if(get_sub_field('button')[0]['enable_button'][0] === 'Yes') {
						$button = get_sub_field('button')[0];

						if($button['button_title'] && $button['button_url']) {
							echo '<a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a>';
						}
					}
				?>
			</div>
			<?php 
				if (get_sub_field('image')) {
					echo '<div class="section-image" style="background-image:url(' . get_sub_field('image')['url'] . ');"></div>';
				}
			?>
		</div>
	<?php endwhile; endif; ?>

	<?php 
		$homeTestimonials = get_field('home_testimonials');
		if($homeTestimonials) {
	?>
		<div id="home-testimonials">
			<div class="container carousel flickity-fade">
				<?php
					foreach($homeTestimonials as $homeTestimonial) {
						echo '<p class="carousel-cell">"' . $homeTestimonial['testimonial'] . '"</p>';
					}
				?>
			</div>
		</div>
	<?php } ?>	
	
	<?php if( have_rows('home_products') ): while ( have_rows('home_products') ) : the_row(); 
		
		$homeProducts = get_sub_field('product_pod');
		$productsImage = get_sub_field('featured_image'); 
	?>

		<div id="home-products">
			<?php
				if($homeProducts) {
					?>
					<div id="products-wrapper" class="container-large">
						<h2>Product types</h2>
						<?php 
							$i = 1;

							foreach($homeProducts as $homeProduct) {

								$productItem = $homeProduct['type'][0];

								echo '
									<div class="product-type">
										' . ($homeProduct['icon']['url'] ? '<img src="'. $homeProduct['icon']['url'] .'" />' : '') .'
										' . ($productItem->name ? '<h3>0'. $i . '. ' . $productItem->name . '</h3>' : '') . '
										<ul class="product-listing">';

										$args = array( 
											'post_type' => 'products', 
											'posts_per_page' => -1,
											'orderby' => 'title',
											'order' => 'ASC',
											'tax_query' => array(
												array(
													'taxonomy' => 'category',
													'field' => 'slug',
													'terms' => $productItem->slug
												)
											)
										);

										$query = new WP_Query($args);
										$products = $query->get_posts();

										foreach($products as $product) {

											echo '<li><a href="' . $product->guid .'">' . $product->post_title . '</a></li>';
										}
										

									echo '</ul>
									</div>';

									$i++;
							}
						} ?>
				</div>

			<?php
				echo '<div class="section-image" style="background-image: url(' . $productsImage["url"] . ');"></div>';
			?>
			
		</div>

	<?php endwhile; endif; ?>
	
	<?php if( have_rows('home_services') ): while ( have_rows('home_services') ) : the_row();
		$servicesPod = get_sub_field('service_pod');

	?>
	<div id="home-services">
		<div class="container">
			<h2>Our Services</h2>
			<?php
	            if ($servicesPod) { ?>

	            <div id="services-wrapper">
	               <?php $i = 1;
	                foreach ($servicesPod as $servicePod) {

	                   	echo '
	                   	<div class="service">
	                   		<div class="service-content">
		                   		<img src="' . $servicePod['icon']['url'] .'" />
		                   		<h3>0' . $i . '. ' . $servicePod['title'] . '</h3>
		                   		<p>' . $servicePod['content'] . '</p>
		                   	</div>
	                   	</div>';

	                    $i++;
	                } 
	               ?>
	            </div>
	  
			<?php  	if(get_sub_field('button')[0]['enable_button'][0] === 'Yes') {
						$button = get_sub_field('button')[0];

						if($button['button_title'] && $button['button_url']) {
							echo '<div class="button-container"><a class="button" href="' . $button['button_url'] . '">' . $button['button_title'] . '</a></div>';
						}
					}
				} ?>
		</div>
	</div>
	<?php endwhile; endif; ?>

	<?php if( have_rows('home_projects') ): while ( have_rows('home_projects') ) : the_row(); 
		$projectText = get_sub_field('featured_text');
		$projectImage = get_sub_field('featured_image');
		$projectUrl = get_sub_field('projects_page_url');
		$projectPods = get_sub_field('project_pod');

		if($projectPods) {
	?>
		<div id="home-projects" class="projects-slider">
			<?php
			echo '
				<a href="' . $projectUrl . '" id="projects-image" class="section-image" style="background-image: url('. $projectImage['url'] .');">
					<p>' . $projectText . '</p> 
				</a>';

			?>
			<div class="container-large">
				<div class="triangle triangle-left triangle-dark"></div>
				<div class="triangle triangle-right triangle-dark"></div>
				<h2>Featured work</h2>
				<?php
					if($projectPods) {
						$x = count($projectPods);
						$i = 1;
						echo '<div class="carousel flickity-fade">';
							foreach($projectPods as $projectPod) {
								echo '
									<div class="carousel-cell">
										<p class="project-counter"><span>0' . $i .' | </span> 0' . $x . '</p>
										<h3>' . $projectPod['title'] . '</h3>
										<p>' . $projectPod['content'] . '</p>
										<a class="button" href="' . $projectPod['url'] . '">VIEW PROJECT</a>
									</div>
								';

								$i++;
							}
						echo '</div>';
					}
				?>
			</div>
		</div>
	<?php } endwhile; endif; ?>

			<?php 
				$homePartners = get_field('home_partners');

				if ($homePartners) {
					echo '<div id="home-partners">
							<div class="container-large carousel">';
				

					foreach($homePartners as $homePartner) {
						echo '<div class="carousel-cell"><img src="' . $homePartner['logo']['url'] . '" /></div>';
					}

					echo '</div></div>';
				}
			?>
		</div>
	</div>


	<div id="home-contact">
		<div class="container-large">
			<h2>Contact Us</h2>
			<?php echo'
				'. (get_field('contact_address', 'options') ? '<p>' . get_field('contact_address', 'options') . '</p>' : '') . '
				'. (get_field('contact_phone', 'options') ? '<p><a href="tel:+61' . get_field('contact_phone', 'options') . '">(+61) ' . get_field('contact_phone', 'options') . '</a></p>' : '') . '
				'. (get_field('contact_email', 'options') ? '<p><a href="mailto:' . get_field('contact_email', 'options') . '">' . get_field('contact_email', 'options') . '</a></p>' : '') . '
				<h3>Open hours</h3>
				'. (get_field('open_hours', 'options') ? '<p>' . get_field('open_hours', 'options') . '</p>' : '') . '
			'; ?>
		</div>
		<div id="contact-map"></div>
	</div>
</main>

<?php
get_template_part('template-parts/map', 'page');
get_footer();
?>
